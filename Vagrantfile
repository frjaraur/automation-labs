# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'yaml'
require 'fileutils'
require 'shellwords'

class String
    def black;          "\e[30m#{self}\e[0m" end
    def red;            "\e[31m#{self}\e[0m" end
    def cyan;           "\e[36m#{self}\e[0m" end
end

proxy = ''

config = YAML.load_file(File.join(File.dirname(__FILE__), 'config.yml'))

base_box=config['environment']['base_box']


domain=config['environment']['domain']

boxes = config['boxes']

boxes_hostsfile_entries=""

role=''

########

boxes.each do |box|
  boxes_hostsfile_entries=boxes_hostsfile_entries+box['mgmt_ip'] + ' ' +  box['name'] + ' ' + box['name']+'.'+domain+'\n'
end

update_hosts = <<SCRIPT
    echo "127.0.0.1 localhost" >/etc/hosts
    echo -e "#{boxes_hostsfile_entries}" |tee -a /etc/hosts
SCRIPT


$install_engine = <<SCRIPT
  #curl -sSk $1 | sh
  DEBIAN_FRONTEND=noninteractive apt-get remove -qq docker docker-engine docker.io
  DEBIAN_FRONTEND=noninteractive apt-get update -qq
  DEBIAN_FRONTEND=noninteractive apt-get install -qq \
  apt-transport-https \
  ca-certificates \
  curl \
  software-properties-common
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | DEBIAN_FRONTEND=noninteractive apt-key add -
  add-apt-repository \
  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) \
  stable"
  DEBIAN_FRONTEND=noninteractive apt-get -qq update
  DEBIAN_FRONTEND=noninteractive apt-get install -y $1
  usermod -aG docker vagrant 2>/dev/null
SCRIPT

$enable_experimental_features = <<SCRIPT
    echo '{"experimental" : true}'> /etc/docker/daemon.json
    systemctl restart docker
SCRIPT


Vagrant.configure(2) do |config|
  config.ssh.shell = "bash -c 'BASH_ENV=/etc/profile exec bash'"
  if Vagrant.has_plugin?("vagrant-proxyconf")
    if proxy != ''
        puts " Using proxy"
        config.proxy.http = proxy
        config.proxy.https = proxy
        config.proxy.no_proxy = "localhost,127.0.0.1"
    end
  end
  config.vm.box = base_box
  boxes.each do |node|
    config.vm.define node['name'] do |config|
      config.vm.hostname = node['name']
      role = node['role']
      config.vm.provider "virtualbox" do |v|
        v.customize [ "modifyvm", :id, "--uartmode1", "disconnected" ]        
        v.name = node['name']
        v.customize ["modifyvm", :id, "--memory", node['mem']]
        v.customize ["modifyvm", :id, "--cpus", node['cpu']]
	    v.customize ["modifyvm", :id, "--macaddress1", "auto"]
        v.customize ["modifyvm", :id, "--nictype1", "Am79C973"]
        v.customize ["modifyvm", :id, "--nictype2", "Am79C973"]
        v.customize ["modifyvm", :id, "--nictype3", "Am79C973"]
        v.customize ["modifyvm", :id, "--nictype4", "Am79C973"]
        v.customize ["modifyvm", :id, "--nicpromisc1", "allow-all"]
        v.customize ["modifyvm", :id, "--nicpromisc2", "allow-all"]
        v.customize ["modifyvm", :id, "--nicpromisc3", "allow-all"]
        v.customize ["modifyvm", :id, "--nicpromisc4", "allow-all"]
        v.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
	      

      end

      config.vm.network "private_network",
      ip: node['mgmt_ip'],
      virtualbox__intnet: "CHEF"


      config.vm.network "public_network",
      bridge: ["enp4s0","wlp3s0","enp3s0f1","wlp2s0"],
      auto_config: true


      config.vm.provision "shell", inline: <<-SHELL
        apt-get update -qq && apt-get install -qq chrony curl && timedatectl set-timezone Europe/Madrid
      SHELL

      config.vm.provision :shell, :inline => update_hosts

      puts "Role "+role
      if role == 'workstation'
        config.vm.network "forwarded_port", guest: 8000, host: 8000, auto_correct: true
        config.vm.provision "shell", inline: <<-SHELL
            curl -o /tmp/chefdk.deb -skL https://packages.chef.io/files/stable/chefdk/2.5.3/ubuntu/16.04/chefdk_2.5.3-1_amd64.deb
            dpkg -i /tmp/chefdk.deb
            apt-add-repository -y ppa:ansible/ansible
            apt-get update -qq && apt-get install -qq ansible
        SHELL
      end
      if role == 'server'
        config.vm.provision "shell", inline: <<-SHELL
            mkdir -p /drop || true
            mkdir -p /downloads || true
            curl -o /downloads/chef-server.deb -skL https://packages.chef.io/files/stable/chef-server/12.16.2/ubuntu/16.04/chef-server-core_12.16.2-1_amd64.deb
            dpkg -i /downloads/chef-server.deb
            chef-server-ctl reconfigure
            until (curl -D - http://localhost:8000/_status) | grep "200 OK"; do sleep 15s; done
            while (curl http://localhost:8000/_status) | grep "fail"; do sleep 15s; done
            chef-server-ctl user-create chefadmin Chef Admin admin@4thcoffee.com insecurepassword --filename /drop/chefadmin.pem
            chef-server-ctl org-create 4thcoffee "Fourth Coffee, Inc." --association_user chefadmin --filename 4thcoffee-validator.pem

        SHELL
      end


      #config.vm.provision "shell" do |s|
       			#s.name       = "Install Docker Engine version "+engine_version
        		#s.inline     = $install_engine
            #s.args       = engine_package
      #end



    end
  end

end
